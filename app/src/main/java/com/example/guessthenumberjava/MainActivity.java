package com.example.guessthenumberjava;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements MainActivityView {
    Player player;
    Player computer;
    Presenter presenter;
    TextView tvWinner;
    Button btnGuess;
    EditText etPlayerChoice;
    Button btnStart;
    EditText etPlayerName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvWinner = findViewById(R.id.tv_winner);
        btnGuess = findViewById(R.id.btn_guess);
        etPlayerChoice = findViewById(R.id.et_player_choice);
        btnStart = findViewById(R.id.btn_start);
        etPlayerName = findViewById(R.id.et_player_name);

        presenter = new Presenter();
        presenter.setView(this);

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                player = new Player(etPlayerName.getText().toString());
                computer = new Player("Computer");

                presenter.setPlayer1(player);
                presenter.setPlayer2(computer);
                presenter.startNextGame();

                etPlayerChoice.setText("");
                tvWinner.setText("START");

                btnStart.setVisibility(View.GONE);
                etPlayerName.setVisibility(View.GONE);

                btnGuess.setVisibility(View.VISIBLE);
                etPlayerChoice.setVisibility(View.VISIBLE);
            }
        });

        btnGuess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int playerChoice = Integer.parseInt(etPlayerChoice.getText().toString());
                if(playerChoice < 0 || playerChoice > 9){
                    tvWinner.setText("Please input number between 0-9");
                }else{
                    presenter.checkTheResult(Integer.parseInt(etPlayerChoice.getText().toString()), presenter.randomNumber());
                }
            }
        });
    }

    @Override
    public void setWinner(String playerName) {
        tvWinner.setText(playerName);
        btnGuess.setVisibility(View.GONE);
        etPlayerChoice.setVisibility(View.GONE);

        btnStart.setVisibility(View.VISIBLE);
        etPlayerName.setVisibility(View.VISIBLE);
    }

    @Override
    public void setNotification(String notification) {
        tvWinner.setText(notification);
    }
}