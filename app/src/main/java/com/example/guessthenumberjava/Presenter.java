package com.example.guessthenumberjava;

import java.util.Random;

public class Presenter {
    private Player player1;
    private Player player2;
    private MainActivityView view;
    private int round = 0;

    void setView(MainActivityView view){
        this.view = view;
    }

    void setPlayer1(Player player){
        this.player1 = player;
    }

    void setPlayer2(Player player){
        this.player2 = player;
    }

    void startNextGame(){
        this.round = 0;
    }

    int randomNumber(){
        Random rand = new Random();
        return rand.nextInt(10);
    }

    void checkTheResult(int player1Choice, int player2Choice){
        round++;
        String winner = "Round "+round+"\n";
        int number = randomNumber();
        player1.playerChoice = player1Choice;
        player2.playerChoice = player2Choice;

        int player1Diff = Math.abs(player1.playerChoice - number);
        int player2Diff = Math.abs(player2.playerChoice - number);

        if(player1Diff < player2Diff){
            winner += "The number is "+number+"\n"+
                    player1.playerName+" choose "+player1.playerChoice+"\n"+
                    player2.playerName+" choose "+player2.playerChoice+"\n"+
                    player1.playerName+" win this round\n";
            player1.playerScore++;
        }else if(player2Diff < player1Diff){
            winner += "The number is "+number+"\n"+
                    player1.playerName+" choose "+player1.playerChoice+"\n"+
                    player2.playerName+" choose "+player2.playerChoice+"\n"+
                    player2.playerName+" win this round\n";
            player2.playerScore++;
        }else{
            winner += "The number is "+number+"\n"+
                    player1.playerName+" choose "+player1.playerChoice+"\n"+
                    player2.playerName+" choose "+player2.playerChoice+"\n"+
                    "Draw\n";
        }

        if(round < 3){
            view.setNotification(winner+"Please input your next number");
        }else{
            checkTheWinner(winner);
        }

    }

    void checkTheWinner(String winner){
        winner += player1.playerName+" score is "+player1.playerScore+"\n"+
                player2.playerName+" score is "+player2.playerScore+"\n";
        if(player1.playerScore > player2.playerScore){
            view.setWinner(winner+player1.playerName + " Win The Game");
        }else if(player1.playerScore < player2.playerScore){
            view.setWinner(winner+player2.playerName + " Win The Game");
        }else{
            view.setWinner(winner+"Draw");
        }
    }
}
