package com.example.guessthenumberjava;

public interface MainActivityView {
    void setWinner(String playerName);
    void setNotification(String notification);
}
